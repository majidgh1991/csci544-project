import sys

nltkFile = open(sys.argv[1], encoding="latin1")
tibFile = open(sys.argv[2], "w")
tibFile.write("q" + "\n")
begin = True
for line in nltkFile:
    if "->" not in line:
        continue
    args = line.split("->")
    lefthand = args[0].strip()
    prob = ""
    if '#' in args[1]:
        prob = " " + args[1][args[1].index('#'):].strip()
        args[1] = args[1][0:args[1].index('#')-1].strip()
    if "|" in args[1]:
        righthand = args[1].split("|")
    else:
        righthand = [args[1]]
    for elem in righthand:
        if elem == "":
            continue
        elem = elem.strip()
        if "'" in elem or '"' in elem:
            elem = elem.replace("'", "", 2)
            elem = elem.replace('"', "", 2)
            newrh = elem + "\n"
        else:
            newrh = lefthand + "(" + elem + ")" + prob + "\n"
        if begin:
            begin = False
            tibFile.write("q" + "->" + newrh)
        else:
            tibFile.write(lefthand + "->" + newrh)



