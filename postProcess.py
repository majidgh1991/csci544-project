import sys
from evaluation import Rule, getGrammarRules, getRulesWithLeftHand

__author__ = 'Setareh7'

output = open(sys.argv[3] , 'w')

file = open(sys.argv[1])
unaryRules, binaryRules = getGrammarRules(file)
corpus = open(sys.argv[2])

beginRules = {}

noises = 0
for line in corpus:
    if " " not in line:
        if line.strip() not in beginRules:
            beginRules.update({line.strip(): 1})
        else:
            beginRules[line.strip()] += 1
    else:
        noises+=1

sumrules = 0
for key, val in beginRules.items():
    sumrules += val
rules = list()
for opt in beginRules.keys():
    rules.append(Rule('S', [opt], beginRules[opt]/sumrules))
print('noises = ' + str(noises))



for rule in unaryRules + rules:
    print(rule)
    if rule.right[0].startswith('AND'):
        r = getRulesWithLeftHand(rule.right[0] , unaryRules,binaryRules)
        if r == None or len(r)!=1:
            print ( 'not found or more than 1 was found!!!')
        rule.right = r[0].right


print ('*********************************')
for rule in unaryRules + binaryRules + list(rules):
    output.write(str(rule) + '\n')


