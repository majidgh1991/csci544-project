import copy
import re
import math
import sys
import random
__author__ = 'Setareh7'

class Rule:
    def __init__(self, left, right , probability):

        self.left = left
        self.right = right
        self.probability = probability
    def __str__(self):
        return ("%s -> %s #%.3f" %(self.left , " ".join(self.right),self.probability))

    def __repr__(self):
        return self.__str__()

    rulePattern = re.compile("(\S+) -> (\S+( \S+)?) #(\d\.\d+)")


def getRulesWithLeftHand(str,myUnaryRules,myBinaryRules):
    answers = list()
    for rule in myUnaryRules:
        if rule.left == str:
            answers.append(rule)
    for rule in myBinaryRules:
        if rule.left == str:
            answers.append(rule)
    return answers

def generateSentence(str,grammar):
    [unary,binary] = grammar
    if str[0]>='a':
        return str
    rules = getRulesWithLeftHand(str,unary,binary)
    if len(rules) == 0:
        print ( 'errorrrrrr: couldt find any rule to go with ' + str )
    elif len(rules) == 1 :
        ans = ""
        #print('using rule ' + rules[0].__str__())
        for rhs in rules[0].right:
            ans += generateSentence(rhs,grammar) + " "
        return ans[:-1]
    else:
        probs = [r.probability for r in rules]
        if sum(probs) != 1:
            print  ('************************* sum of probabilites is not 0')
        cumulativeProbs = [probs[0]]
        for a in probs[1:]:
            cumulativeProbs.append(cumulativeProbs[-1]+a)
        ra = random.random()
        index = 0
        while(cumulativeProbs[index]<ra):
            index+=1

        #ra = random.randint(0,len(rules)-1)
        rule = rules[index]
        #print('using2 rule ' + rule.__str__()   )
        ans = ""
        for rhs in rule.right:
            ans += generateSentence(rhs,grammar) + " "
        return ans[:-1]

# with open('toy.cfg.setareh') as file:
def getGrammarRules(grammarFile):
    myUnaryRules = list()
    myBinaryRules = list()
    for line in grammarFile:
        #  print ( line)
        g = Rule.rulePattern.match(line.strip())
        if g==None:
            print ('********************************' + line)
        g2 = g.group(2).strip().split(' ')

        if len(g2)==1:
            myUnaryRules.append(Rule(g.group(1), g2 , float(g.group(4).strip())))
        else:
            myBinaryRules.append(Rule(g.group(1), g2 , float(g.group(4).strip())))
    return [myUnaryRules,myBinaryRules]

def parse(tokens , rules):
    [myUnaryRules, myBinaryRules] = rules
    def getRuleWithRightHand(str):
        for rule in myUnaryRules:
            if rule.right == [str]:
                yield rule
    def getName(i,j):
        return "%d_%d"%(i,j)

    back = dict()
    n = len(tokens)
    best = {getName(i,j):{} for i in range(0,n+1) for j in range(i+1,n+1)}
    for i in range(1,n+1):
        for rule in getRuleWithRightHand(tokens[i-1].lower()):
            ind = getName(i-1,i)
            if rule.probability > best.get(ind,{}).get(rule.left,-float('inf')):
                dic = best.get(ind , {})
                dic[rule.left] = rule.probability
                best[ind] = dic
                dic2 = back.get(ind,{})
                dic2[rule.left] = [rule , 0 , tokens[i-1]]
                back[ind] = copy.deepcopy(dic2)

    for l in range(2,n+1):
        for i in range( n-l+1):
            j = i+l
            ind = getName(i,j)
            for k in range(i+1 , j):
                for rule in myBinaryRules:
                    a = best[getName(i,k)].get(rule.right[0],-float('inf'))
                    b = best[getName(k,j)].get(rule.right[1],-float('inf'))
                    if a>-float('inf') and b >-float('inf'):
                        newP = rule.probability + a + b
                        if newP >  best[ind].get(rule.left , -float('inf')):
                            best[ind][rule.left] = newP
                            dic3= back.get(ind,{})
                            dic3[rule.left] = [rule, k]
                            back[ind] = copy.deepcopy(dic3)

    return (best[getName(0,n)] !={})

def getParseRate(corpusList , grammar):
    positive = 0
    count = 0
    for line in corpusList:
        line = line.strip()
        count +=1
        tokens = line.strip().split(' ')
        if parse(tokens,grammar):
            positive+=1
    return positive / float(count)

def generateCorpus(grammar,size):
    return [ generateSentence('S' ,grammar) for a in range(size)]

if __name__ == '__main__':
    sys.setrecursionlimit(100000)
    corpusSize = 500
    learnedGrammar = getGrammarRules(open(sys.argv[1]))
    targetGrammar = getGrammarRules(open(sys.argv[2]))
    print (targetGrammar)
    #learnedGrammarCorpus = generateCorpus(learnedGrammar,corpusSize)

    print ( 'ehlloooo')
    #targetGrammarCorpus = generateCorpus(targetGrammar,corpusSize)
    print ( 'hellooo')
    precision = getParseRate(open(sys.argv[3]),targetGrammar)
    print ('done1')
    recall = getParseRate(open(sys.argv[4]),learnedGrammar)
    print ('done2')
    f_score = 2 * precision * recall / (precision + recall)

    print ( "Recall: %f \nPrecision: %f \nF-score: %f" %(precision,recall,f_score))




