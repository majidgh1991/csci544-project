import math
import re
from decimal import Decimal

def createCorpusFromFile(filePath):
    corpus = list()
    corpusFile = open(filePath, encoding="latin1")
    for sentence in corpusFile:
        sentence = sentence.strip()
        if sentence == "":
            continue
        corpus.append(sentence)
    return corpus

def createT(corpus):
    all_words = set()
    for sentence in corpus:
        words = sentence.split()
        for word in words:
            word = word.strip()
            if word == "":
                continue
            all_words.add(word)
    all_words_list = list(all_words)
    #print(all_words_list)
    T = [[0 for xx in range(len(all_words_list))] for xxx in range(len(all_words_list))]
    for sentence in corpus:
        words = sentence.split()
        for i, word in enumerate(words):
            if i != len(words)-1:
                nextWord = words[i+1]
                word_i = all_words_list.index(word)
                word_j = all_words_list.index(nextWord)
                T[word_i][word_j] += 1
    return T,all_words_list

def normilized(cluster):
    cluster_sum = 0
    for vec in cluster:
        if type(vec) is list:
            cluster_sum += sum(vec)
        else:
            cluster_sum += vec
    if cluster_sum == 0:
        return cluster
    for i in range(len(cluster)):
        if type(cluster[i]) is list:
            for j in range(len(cluster[i])):
                cluster[i][j] /= cluster_sum
        else:
            cluster[i] /= cluster_sum
    return cluster


def addBicluster(rows, cols, cluster_matrix, rule_num , grammar , corpus ):

    sumbicluster = 0
    for row in cluster_matrix:
        sumbicluster += sum(row)
    colssum = [0 for xx in cols]
    for row in cluster_matrix:
        for row_i, el in enumerate(row):
            colssum[row_i] += el

    for i, row_word in enumerate(rows):
        if "ROW"+str(i)+"_rule"+rule_num not in grammar:
            grammar.update({"ROW"+str(i)+"_rule"+rule_num: [{"word": row_word, "prob": 1.0}]})
        else:
            grammar["ROW"+str(i)+"_rule"+rule_num].append({"word": row_word, "prob": 1.0})
    for col_i, col_word in enumerate(cols):
        if "COL_rule"+rule_num not in grammar:
            grammar.update({"COL_rule"+rule_num: [{"word": col_word, "prob": colssum[col_i]/sumbicluster}]})
        else:
            grammar["COL_rule"+rule_num].append({"word": col_word, "prob": colssum[col_i]/sumbicluster})

    for i in range(len(rows)):
        grammar.update({"AND"+str(i)+"_rule"+rule_num: {
            "word": "ROW"+str(i)+"_rule"+rule_num + " " + "COL_rule"+rule_num,
            "prob": 1.0}})

    # TODO: update corpus and replace every occurrence with and rule
    #print ( 'rows ' + str(rows) )
    #print ( cols)
    for row_i, row_word in enumerate(rows):
        for col_word in cols:
            combination = row_word + " " + col_word
            combination1 = row_word + " " + col_word + "$"
            combination2 = row_word + " " + col_word + " "
            for i, sentence in enumerate(corpus):
                if combination in sentence:
                    before = corpus[i]
                    corpus[i] = re.sub(combination1, "AND"+str(row_i)+"_rule"+str(rule_num), corpus[i])
                    corpus[i] = re.sub(combination2, "AND"+str(row_i)+"_rule"+str(rule_num) + " ", corpus[i])
                    # corpus[i] = sentence.replace(combination, "AND"+str(row_i)+"_rule"+str(rule_num))
    extension = [0 for xx in rows]
    for sentence in corpus:
        for i, word in enumerate(rows):
            extension[i] += sentence.count(word+" AND_rule"+rule_num)
    extension = normilized(extension)
    for i in range(len(cluster_matrix)):
        cluster_matrix[i].append(extension[i])
'''
    print("row extention: " + str(extension))
    # check coherency to see if we should add it
    # if extension[1] != 0 and abs(cluster_matrix[0][0]/cluster_matrix[1][0] - extension[0]/extension[1]) < 0.1:
    if getMult(cluster_matrix) > -.2:
        grammar["col_rule"+rule_num].append({"word": "AND_rule"+str(rule_num), "prob": 1})

    # do the same for the columns instead
    extension = [0 for xx in cols]
    for sentence in corpus:
        for i, word in enumerate(cols):
            for j in
            extension[i] += sentence.count("AND_rule"+rule_num+" "+word)
    extension = normilized(extension)
    cluster_matrix.append(extension)

    print("col extention: " + str(extension))
    # check coherency to see if we should add it
    # if extension[1] != 0 and abs(cluster_matrix[0][0]/cluster_matrix[0][1] - extension[0]/extension[1]) < 0.1:
    if getMult(cluster_matrix) > -.2:
        grammar["row_rule"+rule_num].append({"word": "AND_rule"+str(rule_num), "prob": 1})
'''

def createExpContext(rows, cols, corpus):
    expcont = {}
    #TODO: we know from k matrix that each combination is in which sentence
    for row_i, row_word in enumerate(rows):
        for col_i, col_word in enumerate(cols):
            combination = row_word + " " + col_word
            for sent_i, sentence in enumerate(corpus):
                # print(sentence)
                if combination in sentence:
                    # index = sentence.find(combination)
                    for index in [m.start() for m in re.finditer(combination, sentence)]:
                        # print(index)
                        context = ""
                        if index == 0:
                            tokens = [x for x in sentence[index+len(combination):].strip().split() if x != ""]
                            if len(tokens) == 0:
                                context = "None None"
                            elif len(tokens) == 1:
                                context = "None " + tokens[0]
                            else:
                                context = tokens[0] + " " + tokens[1]
                        elif index == len(sentence)-len(combination):
                            tokens = [x for x in sentence[0:index].strip().split() if x != ""]
                            if len(tokens) == 0:
                                context = "None None"
                            elif len(tokens) == 1:
                                context = tokens[0] + " None"
                            else:
                                context = tokens[len(tokens)-2] + " " + tokens[len(tokens)-1]
                        else:
                            lefttokens = [x for x in sentence[0:index].strip().split() if x != ""]
                            righttokens = [x for x in sentence[index+len(combination):].strip().split() if x != ""]
                            context = lefttokens[len(lefttokens)-1] + " " + righttokens[0]
                        if context not in expcont:
                            expcont.update({context: {"sentences": [],
                                "vals": [0 for xx in range(len(rows)*len(cols))],
                                "dict_rep": {}}})
                            expcont[context]["sentences"].append(sent_i)
                            expcont[context]["vals"][row_i*len(cols)+col_i] = sentence.count(combination) ##TODO:check validity
                            expcont[context]["dict_rep"].update({combination: sentence.count(combination)})
                        else:
                            expcont[context]["sentences"].append(sent_i)
                            expcont[context]["vals"][row_i*len(cols)+col_i] += sentence.count(combination) ##TODO:check validity
                            if combination in expcont[context]["dict_rep"]:
                                expcont[context]["dict_rep"].update({combination:
                                        expcont[context]["dict_rep"][combination]+sentence.count(combination)})
                            else:
                                expcont[context]["dict_rep"].update({combination: sentence.count(combination)})
    #                     if str(rows) == "['the', 'a']" or str(rows) == "['a', 'the']":
    #                         print(index)
    #                         print(sentence)
    #                         print(context)
    # if str(rows) == "['the', 'a']":
    #     print(cols)
    #     print(expcont.keys())
    expcont_list = [[0 for xx in range(len(expcont.keys()))] for xxx in range(len(rows)*len(cols))]

    i = 0
    #print ( 'rows ' + str( rows))
    #print ( 'cols ' + str(cols))
    #print ( str(expcont))
    for key, val in expcont.items():
        for j, rep in enumerate(val["vals"]):
            expcont_list[j][i] = rep
        i+=1
    #print ( 'expcont_list' + str(expcont_list))
    return expcont_list



def pretty(grammar):
    result = ""
    output = open('finalGrammar' , 'w')
    for a,b in grammar.items():
        if type(b) is list:
            for c in b:
                output.write( '%s -> %s #' % (a,c['word']) + str(c['prob']) + '\n')
        else:
            output.write( '%s -> %s #' % (a,b.get('word')) + str(b.get('prob')) + '\n')
    output.close()

def getMult( lst):
    ans = Decimal(1)
    soorat = 0
    for a in lst:
        r = sum(a)
        if r == 0:
            return -float("inf")
        soorat = soorat + math.log(pow(r,r))
    #print ( 'frequencies : ' + str(frequencies))
    try:
        for a in range(len(lst[0])):
            b = []
            for c in range(len(lst)):
                b.append(lst[c][a])
            r = sum(b)
            if r == 0:
                return -float("inf")
            soorat = soorat + math.log(pow(r, r))
    except:
        print ( 'we got an exception ')
    totalsum = 0
    makhraj = 0
    for a in range(len(lst)):
        for b in range(len(lst[a])):
            makhraj = makhraj + math.log(pow(lst[a][b], lst[a][b]))
            totalsum += lst[a][b]
    if totalsum == 0:
        return -float("inf")
    makhraj = makhraj + math.log(pow(totalsum,totalsum))
    return soorat - makhraj

def extract(frequencies , labels , corpus):
    allCoherencies = {}
    t_len = len(labels)
    maxCoherence = -float("inf")
    chosenCluster = None
    chosenRow = None
    chosenCol = None
    for a in range(t_len):
        for b in range(a+1,t_len):
            for c in range(t_len):
                for d in range(c+1,t_len):
                    cluster = [ [frequencies[a][c] , frequencies[a][d]] ,[frequencies[b][c] , frequencies[b][d]]]
                    # print ( 'before: ' + str(cluster))
                    row = [labels[a] , labels[b]]
                    column = [labels[c] , labels[d]]

                    cluster = normilized(cluster)
                    # print ( 'after : ' + str(cluster))
                    first = getMult(cluster)
                    if first == -float("inf"):
                        continue

                    majid = createExpContext(row,column,corpus)
                    if len(majid) == 0:
                        continue

                    #if row == column:
                    #    print ('salkdjflaksfjlsaddjflsk' )
                    second = getMult(majid)
                    coherency = first + second
                    if coherency > -float("inf"):
                        allCoherencies[str([row,column])] = [ first , second  , cluster]

                    #print ( 'majid ' + str(majid))
                    #sys.exit(123)
                    #coherency = getMult(cluster)
                    if coherency > maxCoherence:
                        chosenCluster = cluster
                        maxCoherence = coherency
                        chosenCol = column
                        chosenRow = row
    #if maxCoherence > epsilon:
    if maxCoherence > -float("inf"):
        global previousCol
        global previousRow
        previousCol = column
        previousRow = row
        # print(str(chosenRow) + " " + str(chosenCol))
        # print(chosenCluster)
        return chosenRow,chosenCol,chosenCluster , allCoherencies
    return None,None,None, None





if __name__ == "__main__":
    createExpContext(['the', 'a'], ['cat', 'dog'], ["the cat chased the dog"])
#createCorpusFromFile(sys.argv[1])

